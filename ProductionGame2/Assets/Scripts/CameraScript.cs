﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CameraScript : MonoBehaviour
{
    [SerializeField] private RawImage imageDisplay;
    [SerializeField] private TextMeshProUGUI gradingText;

    private static CameraScript instance;
    private Camera camera;
    private bool takeScreenshot = false;

    public static float countdown;
    public static float timer = 4;

    //Just call this when you want to take a screenshot
    public static void TakeScreenShot()
    {
        if(countdown <= 0)
        {
            File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot.png");
            File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot.png.meta");
            instance.takeScreenshot = true;
            countdown = timer;
        }
    }
    
    void Start()
    {
        camera = gameObject.GetComponent<Camera>();
        instance = this;
        countdown = 0;
    }

    private void OnPostRender()
    {
        if(takeScreenshot)
        {
            camera.targetTexture = RenderTexture.GetTemporary(camera.pixelWidth, camera.pixelHeight);
            takeScreenshot = false;
            RenderTexture render = camera.targetTexture;
            Texture2D result = new Texture2D(render.width, render.height, TextureFormat.ARGB32, false);
            Rect rect = new Rect(0, 0, render.width, render.height);
            result.ReadPixels(rect, 0, 0);

            byte[] byteArray = result.EncodeToPNG();
            System.IO.File.WriteAllBytes(Application.dataPath + "/Screenshots/CameraScreenshot.png", byteArray);

            RenderTexture.ReleaseTemporary(render);
            camera.targetTexture = null;

            //Put image on ui
            AssetDatabase.ImportAsset("Assets/Screenshots/CameraScreenshot.png");
            Texture2D texture = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Screenshots/CameraScreenshot.png", typeof(Texture2D));
            imageDisplay.texture = texture;
            Color color = new Color(255, 255, 255, 1);
            imageDisplay.color = color;
        }
    }

    public static void setGradingText(string text)
    {
        string grade = text;
        CameraScript.instance.gradingText.text = grade;
    }

    private void Update()
    {
        //This is just for testing the screenshot functionality
        /*if(Input.GetKeyDown(KeyCode.A)) //Take screenshot
        {
            TakeScreenShot();
        }
        else if (Input.GetKeyDown(KeyCode.Q)) //Retrieve all images
        {
            DatabaseManager.pullAllImages();
        }*/

        if (countdown > 0)
        {
            countdown -= Time.deltaTime;

            if(countdown <= 0) //After image has been displayed on screen for (timer) seconds
            {
                DatabaseManager.saveScreenShot();
                //Remove image from ui
                imageDisplay.texture = null;
                Color color = new Color(255, 255, 255, 0);
                imageDisplay.color = color;

                //Remove grade text
                gradingText.text = "";
            }
        }
    }
}
