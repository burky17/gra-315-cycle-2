﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MySql.Data.MySqlClient;
using System.IO;
using System;
using UnityEngine.UI;

public class DatabaseManager : MonoBehaviour
{
    private const string connectionString = "Database=screenshots;Data Source=localhost;User Id=root;Password=";
    private static int id = 0;
    private static int numFiles = 0;

    public void initialDatabase()
    {
        MySqlConnection con = new MySqlConnection(connectionString);
        string queryString = "CREATE SCHEMA `screenshots`";
        MySqlCommand command = new MySqlCommand(queryString);
        con.Open();
        command.ExecuteNonQuery();
        string query2 = "CREATE TABLE `screenshots`.`image` (`id` INT NOT NULL, `name` VARCHAR(45) NULL, `image` BLOB NULL, PRIMARY KEY(`id`))";
        MySqlCommand command2 = new MySqlCommand(queryString);
        con.Open();
        command2.ExecuteNonQuery();
        command2.Connection.Close();
    }

    public static void saveScreenShot(string name)
    {
        MySqlConnection con = new MySqlConnection(connectionString);

        FileStream stream = new FileStream(Application.dataPath + "/Screenshots/CameraScreenshot.png", FileMode.Open, FileAccess.Read);
        BinaryReader reader = new BinaryReader(stream);
        byte[] image = reader.ReadBytes((int)stream.Length);
        stream.Close();
        reader.Close();

        string queryString = "INSERT INTO images (id, name, image) VALUES(@id, @name, @image)";
        MySqlCommand command = new MySqlCommand(queryString);
        command.Connection = con;
        command.Parameters.Add("@id", MySqlDbType.Int16, 11);
        command.Parameters.Add("@name", MySqlDbType.VarChar, 45);
        command.Parameters.Add("@image", MySqlDbType.Blob);
        command.Parameters["@id"].Value = id;
        command.Parameters["@name"].Value = name;
        command.Parameters["@image"].Value = image;
        con.Open();
        command.ExecuteNonQuery();
        command.Connection.Close();
        ++id;

        File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot.png");
        File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot.png.meta");
    }

    public static void saveScreenShot()
    {
        saveScreenShot("Image");
    }

    public static void pullAllImages()
    {
        MySqlConnection con = new MySqlConnection(connectionString);
        string queryString = "SELECT image FROM images";
        MySqlCommand command = new MySqlCommand(queryString);
        command.Connection = con;
        con.Open();
        MySqlDataReader reader = command.ExecuteReader();

        int i = 1;
        while (reader.Read())
        {
            byte[] buffer = new byte[30000];
            reader.GetBytes(reader.GetOrdinal("image"), 0, buffer, 0, 30000);

            FileStream fs;
            fs = new FileStream(Application.dataPath + "/Screenshots/CameraScreenshot" + i.ToString() + ".png", FileMode.OpenOrCreate, FileAccess.Write);
            fs.Write(buffer, 0, 30000);
            fs.Close();
            ++i;
            ++numFiles;
        }

        command.Connection.Close();
    }

    private void OnApplicationQuit()
    {
        MySqlConnection con = new MySqlConnection(connectionString);
        string queryString = "TRUNCATE TABLE images";
        MySqlCommand command = new MySqlCommand(queryString);
        command.Connection = con;
        con.Open();
        command.ExecuteNonQuery();
        command.Connection.Close();

        File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot.png");
        File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot.png.meta");
        for (int i = 1; i <= numFiles; ++i)
        {
            File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot" + i.ToString() + ".png");
            File.Delete(Application.dataPath + "/Screenshots/CameraScreenshot" + i.ToString() + ".png.meta");
        }
    }
}
