﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class DeathScreenScript : MonoBehaviour
{

    private GameObject player;
    [SerializeField]
    Text gameOverText;
    [SerializeField]
    Text gameOverText2;
    [SerializeField]
    GameObject deathScreenOverlay;
    private bool isActive = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    private void Awake()
    {
        
    }

    public void ScreenStart()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Player>().enabled = false;
        Time.timeScale = 0;
        gameOverText.text = "Game Over";
        gameOverText2.text = "Click to restart";
        deathScreenOverlay.SetActive(true);
        isActive = true;
    }

    // Update is called once per frame
    void Update()
    {
        if(isActive == true && Input.GetMouseButtonDown(0))
        {
            Restart();
        }
    }

    public void Restart()
    {
        Time.timeScale = 1;
        isActive = false;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
