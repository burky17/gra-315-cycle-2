﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostSpawnerScript : MonoBehaviour
{
    public GameObject ghost;

    public float coolDown;

    float timer;

    private GameObject[] ghosts;
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(ghost, new Vector3(transform.position.x, 0.3f, transform.position.z), Quaternion.identity);
        timer = coolDown;
    }

    // Update is called once per frame
    void Update()
    {
        ghosts = GameObject.FindGameObjectsWithTag("Enemy");

        if(timer <= 0)
        {
            StartCoroutine("SpawnGhost");
            timer = coolDown;
        }

        timer -= Time.deltaTime;

        if(ghosts.Length == 0 && coolDown > 2) //If the player kills all ghosts in the scene, decrease the cool down time
        {
            coolDown -= 2f;
            timer = 0f;
        }
    }

    IEnumerator SpawnGhost()
    {
        Instantiate(ghost, new Vector3(transform.position.x, 0.3f, transform.position.z), Quaternion.identity);
        return null;
    }
}
