﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GradingScript : MonoBehaviour
{
    private Vector3 growTo;

    private Vector3 startSize;

    private Vector3 startPosition;

    private GameObject playerOb;

    private bool isShooting;

    public bool grow;

    public string hitString;

    [SerializeField]
    float growSpeed;

    [SerializeField]
    float growLength;

    [SerializeField]
    float growWidth;

    [SerializeField]
    float growHeight;

    // Start is called before the first frame update
    void Start()
    {
        gameObject.GetComponent<Collider>().enabled = false;
        playerOb = GameObject.FindGameObjectWithTag("Player");
        startSize = transform.localScale;
        grow = false;
        isShooting = false;
        growTo = new Vector3(growWidth, growHeight, growLength);
    }

    // Update is called once per frame
    void Update()
    {
        if(grow == false)
        {
            gameObject.GetComponent<Collider>().enabled = false;
            transform.localScale = startSize;
        }
        else
        {
            growNow();
        }

        if (Input.GetMouseButtonDown(0))        
            grow = true;
    }

    public void growNow()
    {
        gameObject.GetComponent<Collider>().enabled = true;
        startPosition = transform.position;
        isShooting = true;
        if(transform.localScale.z < growLength)
        {
            transform.localScale = Vector3.Lerp(transform.localScale, growTo * 2, growSpeed * Time.deltaTime); //Hey lets make sure this works with vertical rotations too
        }
        else
        {
            grow = false;
            transform.position = startPosition;
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Enemy" && isShooting == true)
        {
            IEnumerator hitGhost = collision.gameObject.GetComponent<EnemyBehaviorScript>().Hit();

            if (transform.localScale.z > growLength * .8f || transform.localScale.x > growWidth * .7f) 
            {
                StartCoroutine(hitGhost);
                grow = false;
                hitString = "Not quite";
                CameraScript.setGradingText(hitString);
            }
            else
            {
                collision.gameObject.GetComponent<EnemyBehaviorScript>().KillGhost();
                Debug.Log("Killed one");
                UIManager.instance.UpdateScore();
                hitString = "Good Job";
                CameraScript.setGradingText(hitString);
            }
        }
    }

    public string hitMessage(string message)
    {
        return (message);
    }
}
