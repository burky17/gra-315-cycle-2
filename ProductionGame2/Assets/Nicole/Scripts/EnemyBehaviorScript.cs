﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviorScript : MonoBehaviour
{
    public float speed;

    public float rotateSpeed;

    private GameObject playerOb;

    private Rigidbody enemyRB;

    private float colorDown = 1;

    public bool gotHit;

    private bool isDead = false;

    public Renderer objectRenderer;

    // Start is called before the first frame update
    void Start()
    {
        gotHit = false;
        enemyRB = GetComponent<Rigidbody>();
        playerOb = GameObject.FindGameObjectWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {

        if (gotHit == false) //if the ghost hasn't gotten hit, they should move.
        {
            EnemyMove();
        }

        if(isDead == true)
        {
            gotHit = true;
            colorDown -= .05f;
            if (colorDown < 0f)
            {
                Destroy(gameObject);
            }
            objectRenderer.material.color = new Color(1, 1, 1, colorDown);
        }
    }

    public IEnumerator Hit() //Call when enemy gets hit but doesn't die
    {
        gotHit = true;
        enemyRB.velocity = (transform.position - playerOb.transform.position) / 2;
        enemyRB.AddForce(enemyRB.velocity * .2f * Time.deltaTime, ForceMode.Impulse);
        yield return new WaitForSeconds(1f);
        gotHit = false;
    }

    public void EnemyMove() //Move and rotate towards the player 
    {
        enemyRB.velocity = (playerOb.transform.position - transform.position) * speed * Time.deltaTime;
        Vector3 rotateTo = playerOb.transform.position - transform.position;
        rotateTo.y = 0;
        var lookAt = Quaternion.LookRotation(rotateTo);
        transform.rotation = Quaternion.Slerp(transform.rotation, lookAt, rotateSpeed * Time.deltaTime);
    }

    public void KillGhost()
    {
        isDead = true;
    }

    public IEnumerator GhostDie()
    {
        var enemyRen = gameObject.GetComponent<Renderer>().material.color.a;
        enemyRen -= 0.1f; 
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
