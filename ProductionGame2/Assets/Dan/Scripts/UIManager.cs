﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UIManager : MonoBehaviour
{

    public static UIManager instance;
    [SerializeField] private RectTransform healthBar;
    [SerializeField] private Text healthText;
    [SerializeField] private Text scoreText;
    [SerializeField] private int ghostsCaptured = 0;


    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        healthText.text = "Health: 100";
        scoreText.text = "Ghosts Captured: 0";

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetHealth(float aCurrentHealth)
    {
        healthText.text = "Health: " + aCurrentHealth;
    }

    public void UpdateScore()
    {
        ghostsCaptured++;
        scoreText.text = "Ghosts Captured: " + ghostsCaptured;
    }
}
