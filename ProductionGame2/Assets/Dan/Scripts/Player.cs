﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject playerObject;
    [SerializeField] private int playerHealth = 100;
    [SerializeField] private int ghostDamage = 25;
    [SerializeField] private float flashTimer = 1.0f;
    [SerializeField] private Light cameraFlash; 
    [SerializeField] private float verticalSens = 5.0f;
    [SerializeField] private float horizontalSens = 5.0f;  
    [SerializeField] private GradingScript pictureGrader;
    





    // Start is called before the first frame update
    void Start()
    {
        
        flashTimer = 1.0f;
        cameraFlash.enabled = false;
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = Input.GetAxis("Mouse Y");

        Vector3 currentRotation = transform.localEulerAngles;        

        currentRotation.x -= mouseY * verticalSens;
        currentRotation.y += mouseX * horizontalSens;        


        transform.localEulerAngles = currentRotation;

        if (cameraFlash.enabled)
        {
            if (flashTimer <= 0)
            {
                flashTimer = 1.0f;
                cameraFlash.enabled = false;
            }
            else
                flashTimer -= Time.deltaTime;

        }

        //Take screenshot
        if (Input.GetMouseButtonDown(0) && CameraScript.countdown <= 0)
        {
            cameraFlash.enabled = true;
            CameraScript.TakeScreenShot();
            pictureGrader.grow = true;
            Debug.Log("Took Screenshot");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {      

        if (collision.transform.tag == "Enemy")
        {
            playerHealth -= ghostDamage;
            if(playerHealth <= 0)
            {
                GameObject.Find("DeathScreen").GetComponent<DeathScreenScript>().ScreenStart();
            }
            UIManager.instance.SetHealth(playerHealth);
            Destroy(collision.gameObject);
        }     

    }

}
